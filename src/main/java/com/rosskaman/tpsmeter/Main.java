package com.rosskaman.tpsmeter;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        final int startUpDelay = this.getConfig().getInt("start-up-delay") * 20;
        final int checkDelay = this.getConfig().getInt("check-delay") * 20;
        final String dataFolder = this.getConfig().getString("data-folder");
        final int checksPerFile = this.getConfig().getInt("checks-per-file");
        final int checksPerSave = this.getConfig().getInt("checks-per-save");

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            int checks;
            List<JSONObject> checksList = new ArrayList<JSONObject>();
            String filePath = null;

            public void run() {
                /*
                 * Get the data
                 */

                JSONObject routineCheck = new JSONObject();

                for(World world: Bukkit.getWorlds()) {
                    JSONArray chunkArray = new JSONArray();
                    String name = world.getName();

                    for(Chunk chunk: world.getLoadedChunks()) {
                        JSONObject chunkObj = new JSONObject();

                        chunkObj.put("x", chunk.getX());
                        chunkObj.put("z", chunk.getZ());
                        chunkObj.put("key", chunk.getChunkKey());

                        chunkArray.put(chunkObj);
                    }

                    routineCheck.put(name, chunkArray);
                }

                double tps = Bukkit.getTPS()[0];
                routineCheck.put("TPS", tps);

                int playerCount = Bukkit.getOnlinePlayers().size();
                routineCheck.put("playerCount", playerCount);

                long time = System.currentTimeMillis();
                routineCheck.put("time", time);

                checksList.add(routineCheck);



                /*
                 * Store the data
                 */
                if(checks >= checksPerFile || filePath == null) {
                    // Get a new file
                    String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    filePath = dataFolder + timeStamp + ".json";
                    checks = 0;
                }


                if(checksList.size() >= checksPerSave) {
                    // Save the stuffs
                    File file = new File(filePath);
                    FileWriter fw;
                    try {
                        fw = new FileWriter(file, true);

                        if(checks == 0) {
                            fw.write("[");
                        }

                        for(int i = 0; i < checksList.size(); i++) {
                            fw.write(checksList.get(i).toString());

                            if(i < checksList.size() - 1) {
                                fw.write(",");
                            }
                        }

                        if(checks + checksList.size() < (checksPerFile - 1)) {
                            fw.write(",");
                        } else {
                            fw.write("]");
                        }

                        fw.close();
                        checks += checksList.size();
                        checksList.clear();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }




            }
        }, startUpDelay, checkDelay);
    }

    @Override
    public void onDisable() {

    }
}

